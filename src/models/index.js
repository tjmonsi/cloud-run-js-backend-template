/**
 * @module
 * @description Model Index
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { processDefinitions } from '../utils/process-definitions';
import Name from './definitions/atomic/name.json';
import UID from './definitions/atomic/uid.json';
import UserData from './definitions/data/user.json';
import UserPostRequest from './definitions/request/user-post.json';
import UserPutRequest from './definitions/request/user-put.json';
import UserPutOneRequest from './definitions/request/user-put-one.json';
import UserDeleteRequest from './definitions/request/user-delete.json';
import UserList from './definitions/data/user-list.json';
import Success from './definitions/atomic/success.json';
import SuccessfulResponse from './definitions/response/successful-response.json';

/** @type {*} */
const preprocessedDef = {
  Name,
  UID,
  Success,
  UserData,
  UserPostRequest,
  UserPutRequest,
  UserPutOneRequest,
  UserDeleteRequest,
  UserList,
  SuccessfulResponse
};

const definitions = processDefinitions(preprocessedDef);

export {
  definitions
};
