declare module 'is-invalid-path';

declare module '*schema.yml' {
  export const schema: SwaggerModelDefinition
  export const response: {
    [key: string]: {
      description: string,
      type: string,
      properties: SwaggerModelDefinition | SwaggerDataDefinition | string
    }
  }
}

declare module '*security.yml'

interface Config {
  [key: string]: string | number | boolean
}

interface ConfigEnv {
  environment: {
    [key: string]: {
      [key: string]: {
        projectId: '',
        keys: Array<string>,
        [key: string]: any
      }
    }
  },
  errors: {}
}

const { RouteShorthandOptions } = require('fastify');

interface SwaggerApiDefinition {
  [key: string]: {
    [key: string]: {
      [key: string]: RouteShorthandOptions
    }
  }
}

interface SwaggerModelDefinition {
  [key: string]: {
    $id: string,
    type: string,
    description?: string,
    required?: Array<string>,
    items?: SwaggerDataDefinition | string,
    properties?: SwaggerDataDefinition | string | {
      [key: string]: string
    },
    value?: string,
    example?: string
  }
}

interface SwaggerDataDefinition {
  [key: string]: {
    type: string,
    description?: string,
    required?: Array<string>,
    items?: SwaggerDataDefinition | string,
    properties?: SwaggerDataDefinition | string,
    value?: string,
    example?: string
  }
}

interface AddFilePrompt {
  destinationpath: string,
  filename: string,
  description: string
}

interface AddDocumentPagePrompt {
  name: string,
  destinationpath: string | null,
  isIndex: boolean,
  weight: number
}
